# Tp Docker Hugues Marti

Affichage d'une table de base de donnée mysql sur un serveur Apache.

## Installastion 

    git clone git@gitlab.com:FlintD12/tp-docker.git

## Initialisation
    
    cd tp-docker
    docker-compose up

## Aperçu 

<br>

Pour avoir un apercu de la page php affichant le contenu de la table `Users` de la BDD `myDb`, ouvrez votre navigateur favori à l'adresse <http://localhost:8001/>


## Base de donnée

<br>

Pour accéder à la base de donnée via `phpmyadmin`, ouvrez votre navigateur favori à l'adresse <http://localhost:8000/> 
* username : admin
* password : test    
