


CREATE TABLE `Users` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL
);


INSERT INTO `Users` (`id`, `name`) VALUES
(1, 'Hugues'),
(2, 'Baptiste'),
(3, 'Alexandre'),
(4, 'Maxime'),
(5, 'Yeye'),
(6, 'Pascaline'),
(7, 'Benoit');
